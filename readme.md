## Documentation de l'Application

Ce dépôt contient le code source de l'application.

### Accès au dépôt Git

Le lien vers le dépôt Git est [https://gitlab.com/ibtissamcom1993/genie_logiciel.git](lien_vers_le_depot_git).

Le code se trouve dans la branche `master`.

## Résumé de l'Application

L'application permet de scanner des répertoires locaux ou des buckets Amazon S3 pour récupérer des informations sur les fichiers. Elle offre des fonctionnalités telles que la limitation de la profondeur de la recherche, le filtrage par nom de fichier et par type de fichier. De plus, elle fournit des endpoints HTTP pour interagir avec l'application et exécuter des actions telles que la création de scans et le redémarrage de scans existants.

## Prérequis

Avant d'exécuter l'application, assurez-vous d'avoir installé les éléments suivants :
- Java Development Kit (JDK)
- Maven
- Un système de gestion de base de données PostgreSQL (si nécessaire)

## Structure de l'Application

L'application est divisée en plusieurs composants :
- `Component` : Interface décrivant les opérations de scan.
- `Directory` : Implémentation de l'interface `Component` pour représenter les répertoires.
- `FileSystem` : Interface définissant les opérations de scan pour différents systèmes de fichiers.
- `LocalFS` : Implémentation de l'interface `FileSystem` pour scanner des répertoires locaux.
- `S3FS` : Implémentation de l'interface `FileSystem` pour scanner des buckets Amazon S3.
- Tests : Tests unitaires pour valider le fonctionnement des différentes fonctionnalités de l'application.

## Exécution des Tests

Pour exécuter les tests, veuillez utiliser la commande suivante :

```bash
./mvnw clean test
```


## Exécution de l'Application

Pour exécuter l'application, utilisez la commande suivante :

```bash
./mvnw spring-boot:run
```


## Endpoints de l'Application

### Créer un Scan

Pour créer un scan, utilisez l'endpoint suivant :

```bash
POST http://localhost:7070/scans/scan?path=chemin&maxDepth=profondeur&maxFiles=nbFichiers&fileFilter=filtreNom&typeFilter=filtreType&systemType=typeSysteme
```
- `path` : Chemin du répertoire à scanner.
- `maxDepth` : Profondeur maximale de la recherche.
- `maxFiles` : Nombre maximal de fichiers à scanner.
- `fileFilter` : Filtre pour le nom des fichiers (laissez vide pour ne pas filtrer par nom).
- `typeFilter` : Filtre pour le type des fichiers (laissez vide pour inclure tous les types).
- `systemType` : Type de système (LOCAL ou S3).

### Relancer un Scan

Pour relancer un scan, utilisez l'endpoint suivant :

```bash
POST http://localhost:7070/scans/{idScan}/replay?systemType=typeSysteme
```
- `idScan` : ID du scan à relancer.
- `systemType` : Type de système (LOCAL ou S3).

## Utilisation de Spring Boot

L'application utilise le projet Spring Boot pour faciliter le développement, la configuration et le déploiement des applications Java.

## Configuration de la Base de Données

Les configurations avec la base de données se trouvent dans le fichier `application.properties`.

## Déploiement sur AWS

- Pull l'image Docker de l'application à partir du Docker Hub Registry en utilisant la commande suivante :
   ```bash
   docker pull cyril77/scan_app
  ```
- Créez un conteneur Docker à partir de l'image téléchargée 
- Pour accéder à la base de données à partir du terminal, utilisez la commande suivante:
```bash
psql -h scan-instance.c58yo484ozui.us-east-1.rds.amazonaws.com -p 5432 -U postgres -d scan_db

```



### Authors 
    - LOUKILI IBTISSAM
    - Cyrille NEBANGA

